import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { NavComponent } from './nav/nav.component';

import {MatTableModule} from '@angular/material/table';//מטירייל-טבלה

import { AngularFireModule } from '@angular/fire';//פיירבייס
import { AngularFireDatabaseModule } from '@angular/fire/database';//פיירבייס
import { AngularFireAuthModule } from '@angular/fire/auth';//פיירבייס
import { environment } from '../environments/environment'; //פיירבייס

import { RouterModule, Routes } from '@angular/router';//ראוטר

import {MatCardModule} from '@angular/material/card';//לנאב
import {MatInputModule} from '@angular/material/input';//לטופס התחברות
import{FormsModule} from '@angular/forms'; //לטופס
import {BrowserAnimationsModule } from '@angular/platform-browser/animations';//לטוס- אבל להוסיף תמיד
import {MatDialogModule} from '@angular/material/dialog';//פופאפ
import {MatFormFieldModule} from '@angular/material/form-field';//פופאפ

import {MatButtonModule} from '@angular/material/button';//כפתורים
import { MatGridListModule } from '@angular/material/grid-list';//טבלה

import { MatSelectModule } from '@angular/material/select';//לסינון

import { MatCheckboxModule } from '@angular/material/checkbox';
import { BookComponent } from './book/book.component';
import { BooksComponent } from './books/books.component';//צק בוקס

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    NavComponent,
    BookComponent,
    BooksComponent
  ],
  imports: [
    BrowserModule,//מובנה
    MatTableModule, //מטירייל- טבלה
    AngularFireModule.initializeApp(environment.firebase), //פיירבייס
    AngularFireDatabaseModule,//פיירבייס
    AngularFireAuthModule,//פיירבייס
    MatCardModule, //לנאב
    MatInputModule,//להתחברות
    FormsModule,//לטופס
    BrowserAnimationsModule, //לטופס אבל להוסיף תמיד
    MatDialogModule,//פופאפ
    MatFormFieldModule,//פופאפ
    MatButtonModule,//כפתורים
    MatGridListModule,
    MatSelectModule,//סינון
    MatCheckboxModule,

    RouterModule.forRoot([
      {path:'',component:BooksComponent}, //מכיל יו-אר-אל אם הוא ריק מדובר באינדקס בקומפוננט נרשום את הקומפוננטים שמשתנים
      {path:'register',component:RegisterComponent},
     // {path:'codes',component:CodesComponent},
      {path:'login',component:LoginComponent},  
      {path:'books',component:BooksComponent},
      {path:'**',component:BooksComponent}, ])
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
