import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:string;
  password:string;
  nickname:string;
  error = '';
  output = '';
  flag = false;
  
  login()
  {
    this.authService.login(this.email,this.password)
    .then(user =>
    {
      this.router.navigate(['/books'])
    }).catch(err => {
      this.error = err.code;
      this.output = err.message;
      this.flag = true;
    })
  }






  constructor(private authService:AuthService, private router:Router, private afAuth:AngularFireAuth) { }

  ngOnInit() {
  }

}
