import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { flush } from '@angular/core/testing';
import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';


@Component({
  selector: 'book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
@Input() data:any;
@Output() myButtonClicked= new EventEmitter<any>();
name:string;
authorName:string;
key:string;
reed:boolean;
showEdit=false; //כדיפולט לא רואים עריכה
tempName:string;
tempAuthorName:string;

showAN=false; // דיפוךט לא להראות את שם הסופר

showAuthorName(){
  this.showAN=true;
  console.log("show");
}

hideAuthorName(){
  this.showAN=false;
}

edit(){
  this.showEdit=true;
  this.tempName=this.name;
  this.tempAuthorName=this.authorName;

}

Cancel(){
  this.showEdit=false;
  this.name=this.tempName
  this.authorName=this.tempAuthorName;
  this.showAN=false;
}



  constructor(private authService:AuthService, private db:AngularFireDatabase,) { }
  
  isReed(){
    this.authService.user.subscribe(user=>{
      this.db.list('/user/'+user.uid+'/book').update(this.key, {'reed':this.reed})
     }
   )
 }

 Save(){
this.authService.user.subscribe(user=>{
  this.db.list('/user/'+user.uid+'/book').update(this.key,{'name':this.name,'authorName':this.authorName})
})
this.showAN=false;
}

  ngOnInit() {
    this.key=this.data.$key;
    this.authorName=this.data.authorName;
    this.name=this.data.name;
    this.reed=this.data.reed;
  }

}
