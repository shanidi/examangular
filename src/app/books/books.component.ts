import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';//נחוץ כי רק באמצעותו אפשר להגיכ ליוזר ID
import {Router} from "@angular/router";
import { AngularFireDatabase } from '@angular/fire/database';//נוסיף



@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  name:string;
  authorName:string;
  key:string;
  reed:boolean;
  books=[];

  constructor(private router:Router, public authService:AuthService, private db:AngularFireDatabase) { }


  addBook(name:string,authorName:string){
    this.authService.user.subscribe(user=>{ //מגיעים ליוזר
      this.db.list('/user/'+user.uid+'/book/').push({'name':name, 'authorName':authorName});//דוחפים לפיירבייס
    this.name='';// מרוקן את התיבה
    this.authorName='';// מרוקן את התיבה
    }
  )
}

ngOnInit() {
  this.authService.user.subscribe(user=>
  {
    user.uid
    this.db.list('/user/'+user.uid+'/book/').snapshotChanges().subscribe(
      books=>{
        this.books=[];
          books.forEach(
            book =>{
              let b=book.payload.toJSON();
             b["$key"]=book.key;
              this.books.push(b);
             }
         )
      }
    )

  })
}

}
